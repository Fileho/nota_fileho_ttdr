function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


function Run(self, units, parameter)
	
	if not Spring.ValidUnitID(parameter.atlas) or not Spring.ValidUnitID(parameter.unit) then
		return FAILURE
	end

	if Spring.GetUnitTransporter(parameter.unit) ~= nil then
		return SUCCESS
	end

	if not self.init then
		Spring.GiveOrderToUnit(parameter.atlas, CMD.LOAD_UNITS, {parameter.unit}, {"shift"})
		self.init = true
	end

	return RUNNING
end


function Reset(self)
	self.init = false
end
