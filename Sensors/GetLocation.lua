local sensorInfo = {
	name = "GetLocation",
	desc = "Returns a unit loc",
	author = "Fileho",
	date = "2021-07-22",
	license = "none",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(unitID)
	x,y,z = Spring.GetUnitPosition(unitID)
	return {x = x,y = y,z = z}
end